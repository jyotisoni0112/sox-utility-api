package com.sox.utility.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "sox_user_role_details")
@Data
public class UserRoleDetails {

    @Id
    @Column(name = "user_role_id", columnDefinition = "serial")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userRoleId;
    @Column(name = "user_id")
    private String userId;
    @Column(name = "role_id")
    @NotBlank(message = "Role ID is missing")
    private int roleId;
    @Column(name = "app_id")
    @NotBlank(message = "Application ID is missing")
    private String appId;
    @Column(name = "app_name")
    private String appName;
    @Column(name = "loc_id")
    private String locId;
    @Column(name = "loc_name")
    private String locName;
    @Column(name = "active")
    private boolean active;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updatedOn;
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserInfo userInfo;

}
