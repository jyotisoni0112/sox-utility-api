package com.sox.utility.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Date;

@Entity
@Table(name = "sox_user_details")
@Data
public class UserDetails {

    @Id
    @Column(name = "user_id")
    private String userId;
    @Column(name = "first_name")
    @NotBlank(message = "First Name is required")
    private String firstName;
    @Column(name = "last_name")
    @NotBlank(message = "Last name is required")
    private String lastName;
    @Column(name = "email")
    @Email(message = "Email is in bad format")
    @NotBlank(message = "Email is required")
    private String email;
    @Column(name = "active")
    private boolean active;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    private Date updatedOn;

}

