package com.sox.utility.entity;
import lombok.Data;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "sox_module_details")
@Data
public class ModuleEntity {

    @Id
    @Column(name = "module_id")
    private Integer moduleId;
    @Column(name = "module_name")
    private String moduleName;
    @Column(name = "super_module_id")
    private int superModuleId;
    @Column(name = "super_module_name")
    private String superModuleName;
    @Column(name = "active_flag")
    private boolean active;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_on")
    private Date createdOn;
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_on")
    private Date updatedOn;
    @OneToOne
    @JoinColumn(name = "created_by", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserInfo createdByUser;
    @OneToOne
    @JoinColumn(name = "updated_by", referencedColumnName = "user_id", insertable = false, updatable = false)
    private UserInfo updatedByUser;


}
