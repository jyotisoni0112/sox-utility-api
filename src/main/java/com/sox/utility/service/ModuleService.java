package com.sox.utility.service;

import com.sox.utility.entity.ModuleEntity;
import com.sox.utility.exception.SOXException;
import com.sox.utility.repository.ModuleRepository;
import com.sox.utility.utility.SOXConstants;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Log4j2
public class ModuleService {

    String className = ModuleService.class.getName();

    @Autowired
    private ModuleRepository moduleRepository;

    public List<ModuleEntity> getAllModuleDetails() throws SOXException {
        String methodName = "getAllModuleDetails";
        List<ModuleEntity> moduleDetails;
        try{
            moduleDetails = moduleRepository.findAll();
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException("Exception while getting module Details", SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return moduleDetails;
    }

    public Map<Integer, Boolean> getAllModuleStatusDetails() throws SOXException {
        String methodName = "getAllModuleStatusDetails";
        Map<Integer, Boolean> moduleDetails;
        try{
            moduleDetails = moduleRepository.findAll()
                    .stream()
                    .collect(Collectors.toMap(ModuleEntity::getModuleId,ModuleEntity::isActive));
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException("Exception while getting module Details", SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return moduleDetails;
    }

    public ModuleEntity toggleModuleState(int moduleId, String username) throws SOXException {
        String methodName = "toggleModuleState";
        ModuleEntity moduleDetails;
        try{
            Optional<ModuleEntity> module = moduleRepository.findById(moduleId);
            if(module.isPresent()){
                module.get().setActive(!module.get().isActive());
                module.get().setUpdatedBy(username);
                module.get().setUpdatedOn(new Date());
                moduleDetails = moduleRepository.save(module.get());
            }else{
                throw new SOXException("Module not found with ID "+moduleId, SOXConstants.STATUS_CODE_NOT_FOUND,
                        SOXConstants.STATUS_MSG_NOT_FOUND, className, methodName);
            }
        }catch (Exception exception){
            log.error(exception);
            throw new SOXException("Exception while toggling module Details", SOXConstants.STATUS_CODE_APPLICATION_ERROR,
                    SOXConstants.STATUS_MSG_APPLICATION_ERROR, className, methodName);
        }
        return moduleDetails;
    }

}
