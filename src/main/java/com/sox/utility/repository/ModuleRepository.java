package com.sox.utility.repository;

import com.sox.utility.entity.ModuleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModuleRepository extends JpaRepository<ModuleEntity, Integer> {
}
