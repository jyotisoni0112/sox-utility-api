package com.sox.utility.repository;

import com.sox.utility.entity.UserRoleDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRoleDetailsRepository extends JpaRepository<UserRoleDetails, Integer> {

    List<UserRoleDetails> findByUserId(String userId);
    List<UserRoleDetails> findByUserIdAndActive(String userId, boolean active);

}
