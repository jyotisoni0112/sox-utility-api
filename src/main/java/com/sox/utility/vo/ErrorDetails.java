package com.sox.utility.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDetails {

    private String errorMsg;
    private String statusCode;
    private String statusMsg;
    private String className;
    private String methodName;
    private String nameSpace;

}
