package com.sox.utility.controller;

import com.sox.utility.entity.ModuleEntity;
import com.sox.utility.exception.SOXException;
import com.sox.utility.payload.SOXResponse;
import com.sox.utility.service.ModuleService;
import com.sox.utility.utility.SOXConstants;
import com.sox.utility.vo.ErrorDetails;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/module")
@Log4j2
public class ModuleController {

    String className = ModuleController.class.getName();

    @Autowired
    private ModuleService moduleService;

    @GetMapping("/details")
    @PreAuthorize("hasAuthority('ADMIN')")
    public SOXResponse<List<ModuleEntity>> getAllModuleDetails(){
        String methodName = "getAllModuleDetails";
        List<ModuleEntity> moduleDetails;
        SOXResponse<List<ModuleEntity>> soxResponse = new SOXResponse<>();
        try{
            moduleDetails = moduleService.getAllModuleDetails();
            soxResponse.setData(moduleDetails);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/status")
    public SOXResponse<Map<Integer, Boolean>> getAllModuleStatusDetails(){
        String methodName = "getAllModuleDetails";
        Map<Integer, Boolean> moduleDetails;
        SOXResponse<Map<Integer, Boolean>> soxResponse = new SOXResponse<>();
        try{
            moduleDetails = moduleService.getAllModuleStatusDetails();
            soxResponse.setData(moduleDetails);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }

    @GetMapping("/toggle/{moduleId}")
    @PreAuthorize("hasAuthority('ADMIN')")
    public SOXResponse<ModuleEntity> toggleModuleStatus(@PathVariable int moduleId){
        String methodName = "toggleModuleStatus";
        ModuleEntity moduleDetails;
        SOXResponse<ModuleEntity> soxResponse = new SOXResponse<>();

        try{
            String username = SecurityContextHolder.getContext().getAuthentication().getName();
            if(username.isEmpty()){
                throw new SOXException("Empty User name",SOXConstants.STATUS_CODE_VALIDATION_ERROR, SOXConstants.STATUS_MSG_VALIDATION_ERROR, className, methodName);
            }
            moduleDetails = moduleService.toggleModuleState(moduleId, username);
            soxResponse.setData(moduleDetails);
            soxResponse.setStatusCode(SOXConstants.STATUS_CODE_SUCCESS);
            soxResponse.setStatusMsg(SOXConstants.STATUS_MSG_SUCCESS);
        }catch (SOXException exception){
            ErrorDetails errorDetails = new ErrorDetails(exception.getErrorMsg(), exception.getStatusCode(), exception.getStatusMsg()
                    ,exception.getClassName(), exception.getMethodName(), SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }catch (Exception exception){
            log.error(exception);
            ErrorDetails errorDetails = new ErrorDetails(exception.getMessage(), SOXConstants.STATUS_CODE_APPLICATION_ERROR, SOXConstants.STATUS_MSG_APPLICATION_ERROR
                    , className, methodName, SOXConstants.NAMESPACE);
            soxResponse.setErrorDetails(errorDetails);
        }
        return soxResponse;
    }
}
