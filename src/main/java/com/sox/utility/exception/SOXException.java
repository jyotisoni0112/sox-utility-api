package com.sox.utility.exception;

import lombok.Getter;

@Getter
public class SOXException extends Exception{

    private static final long serialVersionUID = 1L;
    private final String errorMsg;
    private final String statusCode;
    private final String statusMsg;
    private final String className;
    private final String methodName;

    public SOXException(String errorMsg, String statusCode, String statusMsg, String className, String methodName){
        this.errorMsg = errorMsg;
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
        this.className = className;
        this.methodName = methodName;
    }
}
