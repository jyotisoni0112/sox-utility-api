package com.sox.utility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SOXUtilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SOXUtilityApplication.class, args);
	}

}
